import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Post } from './post.model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  // Base url
  baseurl = 'http://localhost:8080';

  constructor(private http: HttpClient) { }

  // Http Headers
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  // Cria Post
  CreatePost(data): Observable<Post> {
    return this.http.post<Post>(this.baseurl + '/post/', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  } 

   // Ler Post
   GetPost(titulo): Observable<Post> {
    return this.http.get<Post>(this.baseurl + '/post/' + titulo)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }
   // Ler Posts
   GetPosts(): Observable<Post> {
    return this.http.get<Post>(this.baseurl + '/posts')
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }

  // Atualiza Post
  UpdatePost(data): Observable<Post> {
    return this.http.put<Post>(this.baseurl + '/post', JSON.stringify(data), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandl)
    )
  }



   // Error handling
   errorHandl(error) {
    let errorMessage = '';
    if(error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
 }

}
