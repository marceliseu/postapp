import {
  Component,
  OnInit,
  Input,       
  HostBinding
} from '@angular/core';
import { Post } from './post.model';  
import { PostService } from './post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @HostBinding('attr.class') cssClass = 'row';
  @Input() post: Post;

  constructor(
    public postService: PostService
  ){ }

  voteUp(): boolean {
   // this.post.voteUp();
    this.post.votos += 1;
    console.log("titulo sendo votado =" + this.post.titulo + " votos=" + this.post.votos);
    var obj = { titulo: this.post.titulo, descricao: this.post.descricao, votos: this.post.votos };
    this.postService.UpdatePost(obj).subscribe(res => {
      console.log('Post atualizado !')
    });
    
    return false;
  }

  voteDown(): boolean {
    //this.post.voteDown();
    this.post.votos -= 1;
    console.log("titulo sendo votado =" + this.post.titulo + " votos=" + this.post.votos);
    var obj = { titulo: this.post.titulo, descricao: this.post.descricao, votos: this.post.votos };
    this.postService.UpdatePost(obj).subscribe(res => {
      console.log('Post atualizado !')
    });    
    return false;
  }

  ngOnInit() {
  }

}
