export class Post {
  titulo: string;
  descricao: string;
  votos: number;

  constructor(titulo: string, descricao: string, votos?: number) {
    this.titulo = titulo;
    this.descricao = descricao;
    this.votos = votos || 0;
  }

  voteUp(): void {
    this.votos += 1;
  }

  voteDown(): void {
    this.votos -= 1;
  }

  
}
