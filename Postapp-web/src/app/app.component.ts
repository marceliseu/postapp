import { Component } from '@angular/core';
import { Post } from './post/post.model';
import { PostService } from './post/post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  //posts: Post[];
  posts: any = [];
  data_teste = [
    new Post('Tendencias Tecnologicas', 'Blockchain, AI e Cloud', 3),
    new Post('xxx', 'xxxx', 2),
    new Post('zzz', 'zzzz', 1)
  ];

    /*
  constructor() {
    this.posts = [
      new Post('Tendencias Tecnologicas', 'Blockchain, AI e Cloud', 3),
      new Post('xxx', 'xxxx', 2),
      new Post('zzz', 'zzzz', 1)
    ];
  }
     */
  ngOnInit() {
    this.listPosts();
  }

  constructor(
    public postService: PostService
  ){ }

  addPost(titulo: HTMLInputElement, descricao: HTMLInputElement): boolean {
    console.log(`Adding post titulo: ${titulo.value} and descicao: ${descricao.value}`);
    this.posts.push(new Post(titulo.value, descricao.value, 0));
    var obj = { titulo: titulo.value, descricao: descricao.value, votos: 0 };
    this.postService.CreatePost(obj).subscribe(res => {
      console.log('Post adicionado !')
     // this.ngZone.run(() => this.router.navigateByUrl('/issues-list'))
    });
    titulo.value = '';
    descricao.value = '';
    return false;
  }

  listPosts() {
    //this.posts = this.data_teste;
    // Executa serviço para listar Posts
    return this.postService.GetPosts().subscribe((data: {}) => {
      this.posts = data;
    })
  }

  sortedPosts(): Post[] {
    return this.posts.sort((a: Post, b: Post) => b.votos - a.votos);
  }
}
