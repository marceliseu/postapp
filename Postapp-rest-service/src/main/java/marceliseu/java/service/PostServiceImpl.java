package marceliseu.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import marceliseu.java.model.Post;
import marceliseu.java.repository.PostRepository;

@Service
public class PostServiceImpl implements PostService {

    @Autowired
	private PostRepository postRepository;	
    
	@Override
	public void deletaTodosPosts() {
			postRepository.deleteAll();
	}
    
    @Override
    public List<Post> listaPosts() {
    	return postRepository.findAll();
    }

	@Override
	public Post lerPost(String titulo) {
		Post post = postRepository.findByTitulo(titulo);
		return post;	
	}

	@Override
	public void inserePost(Post post) {
		if (post != null) {
			postRepository.save(post);
		}
	}

	@Override
	public void atualizaPost(Post post) {
		if (post != null) {
			// parametros da pagina
			String titulo = post.getTitulo();
			String descricao = post.getDescricao();
			int votos = post.getVotos();
			
			// obtem ID MongoDB e atualiza Obj Post
			post = postRepository.findByTitulo(titulo);
			// {"_id":"5d6f1d72a88f3884b43cb6c6","titulo":"titulo2","descricao":"descricao 2","votos":0,"_class":"marceliseu.java.model.Post"}
			//post.setId("5d6f1d72a88f3884b43cb6c6");
			post.setVotos(votos);
			postRepository.save(post);
		}
	}
}
