package marceliseu.java.service;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import marceliseu.java.model.Post;

public interface PostService {

	List<Post> listaPosts();
	Post lerPost(@PathVariable String titulo);
	void inserePost(@RequestBody Post post);
	void atualizaPost(@RequestBody Post post);
	void deletaTodosPosts();

}
