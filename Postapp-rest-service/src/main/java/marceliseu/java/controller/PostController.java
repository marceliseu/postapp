package marceliseu.java.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import marceliseu.java.model.Post;
import marceliseu.java.service.PostServiceImpl;

@CrossOrigin(maxAge = 3600)
@RestController
public class PostController {
	
	@Autowired
	//private PostRepository postRepository;	
	private PostServiceImpl postService;

    // Lista Posts
    @GetMapping(path = "/posts", produces = "application/json; charset=UTF-8")
    @ResponseBody
    public List<Post>  listaPosts() {     
		return postService.listaPosts();
    }    
 
	// Ler Post
	@GetMapping(path = "/post/{titulo}", produces = "application/json; charset=UTF-8")
	public Post lerPost(@PathVariable String titulo) {
		return postService.lerPost(titulo);
	}    
	
	// Insere Post
	@PostMapping(path = "/post", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public void inserePost(@RequestBody Post post) {
		postService.inserePost(post);
	}  	
	
	// Atualiza Post
	@PutMapping(path = "/post", consumes = {MediaType.APPLICATION_JSON_VALUE})
	public void atualizaPost(@RequestBody Post post) {
		postService.atualizaPost(post);
	}
    

	
}