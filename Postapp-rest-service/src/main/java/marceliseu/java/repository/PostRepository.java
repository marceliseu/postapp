package marceliseu.java.repository;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.repository.MongoRepository;
import marceliseu.java.model.Post;


public interface PostRepository extends MongoRepository<Post, String> {

    public Post findByTitulo(String titulo);


}
