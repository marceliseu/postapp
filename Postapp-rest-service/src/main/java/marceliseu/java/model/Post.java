package marceliseu.java.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("Post")
public class Post {

    @Id
    public String id;
    
    @Indexed(unique = true)
	private String titulo;
	private String descricao;
	private int votos;


	public Post() {
	}
	  
    public Post(String titulo, String descricao, int votos) {
        this.titulo = titulo;
        this.descricao = descricao;
        this.votos = votos;
    }

    @Override
    public String toString() {
        return String.format(
                "Post[titulo=%s, descricao='%s', votos='%s']",
                titulo, descricao, votos);
    }
    
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getVotos() {
		return votos;
	}
	public void setVotos(int votos) {
		this.votos = votos;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

}
