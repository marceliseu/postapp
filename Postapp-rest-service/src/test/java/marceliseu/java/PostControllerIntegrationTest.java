package marceliseu.java;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import marceliseu.java.model.Post;
import marceliseu.java.service.PostServiceImpl;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class PostControllerIntegrationTest  {

    @Autowired
    private MockMvc mvc;
    @Autowired
	private PostServiceImpl postService;
 
    boolean posts_inicializados = false;

    @Before
	public void inicializaPosts() throws Exception {
		System.out.println("inicializando...");
		postService.deletaTodosPosts();
		Post post1 = new Post("titulo1","descricao1",0);
		postService.inserePost(post1);
		Post post2 = new Post("titulo2","descricao2",0);
		postService.inserePost(post2);
		Post post3 = new Post("titulo3","descricao3",0);
		postService.inserePost(post3);
	 
		System.out.println("inicializado");

	}

	
    @Test
    public void verificaSeTem3Posts() throws Exception
    {
      this.mvc.perform( MockMvcRequestBuilders
          .get("/posts")
          .accept(MediaType.APPLICATION_JSON))
      	  .andDo(print())
      	  .andExpect(status().isOk())
      	  .andExpect(jsonPath("$", hasSize(3))); 
    }
    
    @Test
    public void verificaSeDescricao1() throws Exception
    {
      this.mvc.perform( MockMvcRequestBuilders
          .get("/post/titulo1")
          .accept(MediaType.APPLICATION_JSON))
      	  .andDo(print())
      	  .andExpect(status().isOk())
      	  .andExpect(jsonPath("$.descricao", is("descricao1")));    
    }    
    
    @Test(expected = org.springframework.web.util.NestedServletException.class)
    public void testaCriacaoNovoPost() throws Exception
    {
  
		 JSONObject jsonobj = new JSONObject();
		 jsonobj.put("titulo", "titulo1");
		 jsonobj.put("descricao", "descricao1");
		 jsonobj.put("votos", 1);
		 jsonobj.toJSONString().getBytes();
		 
      this.mvc.perform( MockMvcRequestBuilders
          .post("/post")
          .contentType(MediaType.APPLICATION_JSON)
          .content(jsonobj.toJSONString().getBytes())
          .accept(MediaType.APPLICATION_JSON))
      	  .andDo(print())
      	  .andExpect(status().isOk());
    }      
         

}
