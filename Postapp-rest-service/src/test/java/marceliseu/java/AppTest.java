package marceliseu.java;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.*;

import marceliseu.java.model.Post;

class ShortFormatter extends Formatter {
    // this method is called for every log records
    public String format(LogRecord rec) {
    		String msg ;
    		msg = calcDate(rec.getMillis()) + " " + 
    		rec.getSourceMethodName() + ":" + 
    		rec.getMessage() + "\n";;
    		//msg = rec.getMessage() + "\n";
		return msg;
    }
    private String calcDate(long millisecs) {
        //SimpleDateFormat date_format = new SimpleDateFormat("dd MMM yyyy HH:mm");
   	 SimpleDateFormat date_format = new SimpleDateFormat("dd/MM/yy HH:mm");
       Date resultdate = new Date(millisecs);
       return date_format.format(resultdate);
   }
}

public class AppTest {
	
	private final static Logger logger = Logger.getLogger(AppTest.class.getName());
	static private Formatter formatter  = new ShortFormatter();
	
	public AppTest() {
		Logger rootLog = Logger.getLogger("");
		rootLog.setLevel( Level.INFO );
		Handler rootHandler = rootLog.getHandlers()[0];
		rootHandler.setLevel( Level.INFO );   // Default console handler
		rootHandler.setFormatter(formatter);
		
		logger.info("Inicializado " );
	}

	@Test
	public void TestaObjPost()
	{
    	List posts = new ArrayList<Post>();   	
        posts.add(new Post("titulo1", "descrição1", 0));
        posts.add(new Post("titulo2", "descrição2", 2));
        posts.add(new Post("titulo3", "descrição3", 1));
        
        Post post = (Post) posts.get(0);
        System.out.println(post.getTitulo());
        assertEquals("descrição1", post.getDescricao());
	}
	
	@Test 
	public void CriaObjJSON() throws ParseException
	{
         JSONObject jsonobj_posts = new JSONObject();
		 JSONArray jarray = new JSONArray();
		 JSONObject jsonobj = new JSONObject();
		 
		 jsonobj.put("titulo", "titulo1");
		 jsonobj.put("descricao", "descricao1");
		 jsonobj.put("votos", 1);
		 jarray.add(jsonobj);
		 
		 jsonobj.put("titulo", "titulo2");
		 jsonobj.put("descricao", "descricao2");
		 jsonobj.put("votos", 1);
		 jarray.add(jsonobj);

		 jsonobj_posts.put("Posts", jarray);		 
		 System.out.println(jsonobj_posts);

	}
	
	@Test
	public void TestaJSONParser() throws ParseException
	{		 
		 String stringToParse = "{\"titulo\":\"titulo1\",\"votos\":1,\"descricao\":\"descricao1\"}";
		 
		 JSONParser parser = new JSONParser();
		 JSONObject jsonobj_post = (JSONObject) parser.parse(stringToParse);
		 
	        String titulo = (String) jsonobj_post.get("titulo");
	        String descricao = (String) jsonobj_post.get("descricao");
	        long votos = (Long) jsonobj_post.get("votos");
			System.out.println("titulo=" + titulo);
			System.out.println("descricao=" + descricao);
			System.out.println("votos=" + votos);	

	}
}
